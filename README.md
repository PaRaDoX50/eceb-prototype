## ECEB-PROTOTYPE

The Essential Care for Every Baby (ECEB) educational and training program, developed by the American Academy of Pediatrics, provides knowledge, skills, and competencies to nurses and doctors in low/middle-income settings so that they can provide life-saving care to newborns from birth through 24 hours postnatal.

## 🔗 Links

- [APKS](apks/)

## SCREENSHOTS
<img src="screenshots/ss8.jpeg" alt="drawing" width="200"/>
<img src="screenshots/ss7.jpeg" alt="drawing" width="200"/>
<img src="screenshots/ss6.jpeg" alt="drawing" width="200"/>
<img src="screenshots/ss5.jpeg" alt="drawing" width="200"/>
<img src="screenshots/ss9.jpeg" alt="drawing" width="200"/>
<img src="screenshots/ss10.jpeg" alt="drawing" width="200"/>
<img src="screenshots/ss4.jpeg" alt="drawing" width="200"/>
<img src="screenshots/ss11.jpeg" alt="drawing" width="200"/>
<img src="screenshots/ss12.jpeg" alt="drawing" width="200"/>
<img src="screenshots/ss13.jpeg" alt="drawing" width="200"/>

### Device-Preview 
<img src="screenshots/ss14.jpeg" alt="drawing" width="200"/>







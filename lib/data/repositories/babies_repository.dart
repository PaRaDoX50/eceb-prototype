import 'package:eceb_prototype/data/models/baby.dart';

class BabiesRepository {
  Future<List<Baby>> getListOfBabies() async {
    await Future.delayed(Duration(seconds: 2));
    List<Baby> list = [
      Baby(
          gender: "Male",
          hoursFromBirth: 3,
          location: "Paternal Ward",
          parent: "Nia",
          stateOfBaby: StateOfBaby.discharged),
      Baby(
          gender: "Female",
          hoursFromBirth: 4,
          location: "Paternal Ward",
          parent: "Riya",
          stateOfBaby: StateOfBaby.normal),
      Baby(
          gender: "Female",
          hoursFromBirth: 1,
          location: "Paternal Ward",
          parent: "Nia",
          stateOfBaby: StateOfBaby.highRisk),
      Baby(
          gender: "Male",
          hoursFromBirth: 2,
          location: "Paternal Ward",
          parent: "Zia",
          stateOfBaby: StateOfBaby.problem),
      Baby(
          gender: "Male",
          hoursFromBirth: 5,
          location: "Paternal Ward",
          parent: "Poo",
          stateOfBaby: StateOfBaby.discharged),
      Baby(
          gender: "Male",
          hoursFromBirth: 25,
          location: "Paternal Ward",
          parent: "Sia",
          stateOfBaby: StateOfBaby.discharged),
      Baby(
          gender: "Female",
          hoursFromBirth: 15,
          location: "Paternal Ward",
          parent: "Sakshi",
          stateOfBaby: StateOfBaby.discharged),
    ];
    return list;
  }
}

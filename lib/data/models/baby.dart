class Baby{
  final String parent;
  final String location;
  final String gender;
  final int hoursFromBirth;
  final StateOfBaby stateOfBaby;

  Baby(
      {this.parent,
        this.location,
        this.gender,
        this.hoursFromBirth,
        this.stateOfBaby});
}

enum StateOfBaby{
  discharged,
  problem,
  highRisk,
  normal
}
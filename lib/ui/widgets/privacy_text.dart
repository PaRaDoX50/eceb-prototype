import 'package:flutter/material.dart';

class PrivacyText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Text(
        'By continuing, you agree to our Privacy Policies,\n Data Use Policies including our Cookie Use.',
        style: TextStyle(
          fontSize: 8,
          color: const Color(0xff898a8f),
        ),
        textAlign: TextAlign.left,
      ),
    );
  }
}

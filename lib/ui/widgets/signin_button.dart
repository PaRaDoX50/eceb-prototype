import 'package:flutter/material.dart';

class SignInButton extends StatelessWidget {
  final Function onPressed;
  final bool emailPasswordNotEmpty;

  SignInButton(
      {@required this.onPressed, @required this.emailPasswordNotEmpty});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 38,
      width: MediaQuery.of(context).size.width * .4,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: const Color(0xffffffff),
        border: Border.all(
          width: 1.0,
          color: const Color(0xffececec),
        ),
        boxShadow: [
          BoxShadow(
            color: const Color(0x0d000000),
            offset: Offset(0, 2),
            blurRadius: 5,
          ),
        ],
      ),
      child: RaisedButton(
        elevation: 0,
        child: Text(
          "Sign In",
          style: TextStyle(
              color: emailPasswordNotEmpty
                  ? Color(0xff3082CC)
                  : Color(0x693082cc)),
        ),
        onPressed: emailPasswordNotEmpty ? onPressed : () {},
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
          side: BorderSide(color: const Color(0xffececec), width: 1),
        ),
        color: const Color(0xffffffff),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class RoundedTextField extends StatelessWidget {
  final TextEditingController controller;
  final String hintText;
  final Function onChange;
  final bool obscureText;

  RoundedTextField(
      {@required this.controller,
      @required this.hintText,
      @required this.onChange,
      this.obscureText});

  @override
  Widget build(BuildContext context) {
    var mediaQueryWidth = MediaQuery.of(context).size.width;

    return Container(
      width: mediaQueryWidth * .7,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: const Color(0xffffffff),
        border: Border.all(width: 1.0, color: const Color(0xffececec)),
        boxShadow: [
          BoxShadow(
            color: const Color(0x0d000000),
            offset: Offset(0, 2),
            blurRadius: 5,
          ),
        ],
      ),
      child: TextFormField(
        onChanged: (_) => onChange(),
        controller: controller,
        style: TextStyle(
          fontSize: 14,
          color: const Color(0xff707070),
          fontWeight: FontWeight.w500,
        ),
        obscureText: obscureText != null ? obscureText : false,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.symmetric(vertical: 0, horizontal: 16),
          hintText: hintText,
          filled: true,
          fillColor: const Color(0xffffffff),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(25.0),
            borderSide: BorderSide(color: const Color(0xffececec), width: 1),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(25.0),
            borderSide: BorderSide(color: const Color(0xffececec), width: 1),
          ),
        ),
      ),
    );
  }
}

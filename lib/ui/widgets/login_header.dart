import 'package:flutter/material.dart';

class LoginHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var mediaQueryHeight = MediaQuery.of(context).size.height;
    return
      Container(
        width: double.infinity,
        height: mediaQueryHeight * .45,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(34),
              bottomRight: Radius.circular(34)),
          color: Theme.of(context).primaryColor,
        ),
        child: Column(
          children: [
            Flexible(
              flex: 1,
              child: Container(
                child: Image.asset(
                  "assets/images/logo.png",
                ),
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Flexible(
              flex: 1,
              child: Text(
                'ECEB',
                style: TextStyle(
                  fontSize: 85,
                  color: const Color(0xffffffff),
                  fontWeight: FontWeight.w700,
                ),
                textAlign: TextAlign.center,
              ),
            ),

          ],
        ),
      );
  }
}

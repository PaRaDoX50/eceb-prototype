import 'package:flutter/material.dart';

class LoginTypeButton extends StatelessWidget {
  final String buttonText;
  final Function onPressed;

  LoginTypeButton({@required this.buttonText, @required this.onPressed});

  @override
  Widget build(BuildContext context) {
    var mediaQueryHeight = MediaQuery.of(context).size.height;
    var mediaQueryWidth = MediaQuery.of(context).size.width;
    return Container(
      width: mediaQueryWidth * .7,
      height: mediaQueryHeight * .15,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: const Color(0xffffffff),
        border: Border.all(width: 1.0, color: const Color(0xffececec)),
        boxShadow: [
          BoxShadow(
            color: const Color(0x0d000000),
            offset: Offset(0, 2),
            blurRadius: 5,
          ),
        ],
      ),
      child: RaisedButton(
        color: const Color(0xffffffff),
        onPressed: onPressed,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
          side: BorderSide(color: const Color(0xffececec), width: 1),
        ),
        child: Center(
            child: Text(
          buttonText,
          style: TextStyle(color: Color(0xff3082CC)),
        )),
      ),
    );
  }
}

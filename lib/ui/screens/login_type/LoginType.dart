import 'package:eceb_prototype/ui/screens/login_facility/LoginFacility.dart';
import 'package:eceb_prototype/ui/screens/login_individual/LoginIndividual.dart';
import 'package:eceb_prototype/ui/screens/login_type/components/LoginTypeButton.dart';
import 'package:flutter/material.dart';

class LoginType extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var mediaQueryHeight = MediaQuery.of(context).size.height;
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          iconTheme: IconThemeData(color: Colors.white),
        ),
        body: Column(
          children: [
            Container(
              width: double.infinity,
              height: mediaQueryHeight * .4,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(34),
                    bottomRight: Radius.circular(34)),
                color: Theme.of(context).primaryColor,
              ),
              child: Column(
                children: [
                  Flexible(
                    flex: 3,
                    child: Container(
                      child: Image.asset(
                        "assets/images/logo.png",
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  Flexible(
                    flex: 1,
                    child: Text(
                      'Essential Care for Every Baby',
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                        fontWeight: FontWeight.w700,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  LoginTypeButton(
                    buttonText: "Individual",
                    onPressed: () {
                      Navigator.pushNamed(context, LoginIndividual.route);
                    },
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  LoginTypeButton(
                    buttonText: "Facility",
                    onPressed: () {
                      Navigator.pushNamed(context, LoginFacility.route);
                    },
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                'By continuing, you agree to our Privacy Policies,\n Data Use Policies including our Cookie Use.',
                style: TextStyle(
                  fontSize: 8,
                  color: const Color(0xff898a8f),
                ),
                textAlign: TextAlign.left,
              ),
            )
          ],
        ),
      ),
    );
  }
}

import 'package:eceb_prototype/ui/pages/home_page/home_page.dart';
import 'package:eceb_prototype/ui/pages/list_of_babies/list_of_babies.dart';
import 'package:flutter/material.dart';

class MainScreen extends StatefulWidget {
  static String route = "/main_screen";

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _currentIndex = 0;
  final pages = [
    HomePage(),
    ListOfBabies(),
    Center(
      child: Text("Not Implemented"),
    ),
    Center(
      child: Text("Not Implemented"),
    )
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          iconTheme: IconThemeData(color: Colors.white),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(24),
            ),
          ),
          title: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    _currentIndex == 1 ? "List of Babies" : 'ECEB',
                    style: TextStyle(
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    'ID: *****124',
                    style: TextStyle(
                      fontSize: 10,
                      color: const Color(0xffffffff),
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
          bottom: PreferredSize(
            child: Text(""),
            preferredSize: Size.fromHeight(16),
          ),
          actions: [
            Image.asset("assets/images/logo.png"),
            SizedBox(
              width: 16,
            )
          ],
        ),
        body: pages[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          showUnselectedLabels: true,
          currentIndex: _currentIndex,
          type: BottomNavigationBarType.fixed,
          onTap: changeIndex,
          selectedItemColor: Color(0xff064294),
          unselectedItemColor: Color(0xff707070),
          items: [
            BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
            BottomNavigationBarItem(
                icon: Icon(Icons.list), label: "List of Babies"),
            BottomNavigationBarItem(
                icon: Icon(Icons.notifications_none), label: "Notifications"),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: "Profile"),
          ],
        ),
      ),
    );
  }

  changeIndex(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}

import 'package:eceb_prototype/ui/screens/main_screen/main_screen.dart';
import 'package:eceb_prototype/ui/widgets/login_header.dart';
import 'package:eceb_prototype/ui/widgets/privacy_text.dart';
import 'package:eceb_prototype/ui/widgets/rounded_textfield.dart';
import 'package:eceb_prototype/ui/widgets/signin_button.dart';
import 'package:flutter/material.dart';

class LoginFacility extends StatefulWidget {
  static String route = "/login_facility";

  @override
  _LoginFacilityState createState() => _LoginFacilityState();
}

class _LoginFacilityState extends State<LoginFacility> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool emailPasswordNotEmpty = false;

  changeEmailPassBool() {
    setState(() {
      if (_emailController.text.isNotEmpty &&
          _passwordController.text.isNotEmpty) {
        emailPasswordNotEmpty = true;
      } else {
        emailPasswordNotEmpty = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    var mediaQueryHeight = MediaQuery.of(context).size.height;
    var appBar = AppBar(
      elevation: 0,
      iconTheme: IconThemeData(color: Colors.white),
    );

    return SafeArea(
      child: Scaffold(
        appBar: appBar,
        body: SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(
                maxHeight: mediaQueryHeight -
                    appBar.preferredSize.height -
                    MediaQuery.of(context).padding.top),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                LoginHeader(),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      RoundedTextField(
                        controller: _emailController,
                        hintText: "Enter Employee Code",
                        onChange: changeEmailPassBool,
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      RoundedTextField(
                        controller: _passwordController,
                        hintText: "Password",
                        onChange: changeEmailPassBool,
                        obscureText: true,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: SignInButton(
                          onPressed: () {
                            Navigator.pushNamedAndRemoveUntil(
                                context, MainScreen.route, (route) => false);
                          },
                          emailPasswordNotEmpty: emailPasswordNotEmpty,
                        ),
                      ),
                    ],
                  ),
                ),
                PrivacyText(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:eceb_prototype/business_logic/blocs/babies/babies_bloc.dart';
import 'package:eceb_prototype/data/models/baby.dart';
import 'package:eceb_prototype/ui/pages/list_of_babies/components/registered_baby_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListOfBabies extends StatefulWidget {
  @override
  _ListOfBabiesState createState() => _ListOfBabiesState();
}

class _ListOfBabiesState extends State<ListOfBabies> {
  List<String> _filters = [
    'All',
    'Discharged',
    'Problem',
    'Normal',
    'High Risk'
  ]; // Option 2
  String _selectedFiltered;

  @override
  void initState() {
    changeFilter(_filters[0]);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: true,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(24),
              ),
            ),
            backgroundColor: Colors.white,
            automaticallyImplyLeading: false,
            flexibleSpace: Center(
              child: Row(
                children: [
                  Expanded(
                    child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: _getSearchWidget()),
                  ),
                  Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: _getDropdownWidget()),
                ],
              ),
            ),
          ),
          SliverToBoxAdapter(
            child: Padding(
              padding: const EdgeInsets.only(top: 8, bottom: 16, left: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    'Past Registered',
                    style: TextStyle(
                      fontSize: 16,
                      color: const Color(0xff333348),
                      fontWeight: FontWeight.w700,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ],
              ),
            ),
          ),
          BlocBuilder<BabiesBloc, BabiesState>(builder: (context, state) {
            if (state is Loading) {
              return SliverFillRemaining(
                child: Center(
                    child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 8,
                    ),
                    Text("Calling Fake API")
                  ],
                )),
              );
            } else if (state is BabiesListRetrievedState) {
              return SliverList(
                delegate: SliverChildBuilderDelegate((_, index) {
                  return Padding(
                    padding: const EdgeInsets.only(bottom: 8.0),
                    child: RegisteredBabyCard(
                      gender: state.babies[index].gender,
                      hoursFromBirth: state.babies[index].hoursFromBirth,
                      location: state.babies[index].location,
                      parent: state.babies[index].parent,
                      stateOfBaby: state.babies[index].stateOfBaby,
                    ),
                  );
                }, childCount: state.babies.length),
              );
            } else {
              return SliverFillRemaining(
                child: Center(
                    child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(
                      height: 8,
                    ),
                    Text("Calling Fake API")
                  ],
                )),
              );
            }
          }),
        ],
      ),
    );
  }

  Widget _getSearchWidget() {
    return Container(
      child: Row(
        children: [
          SizedBox(
            width: 8,
          ),
          Expanded(
              child: Text(
            'Not Implemented',
            style: TextStyle(
              fontSize: 10,
              color: const Color(0xfff6f6f6),
              fontWeight: FontWeight.w600,
            ),
            textAlign: TextAlign.left,
          )),
          Icon(
            Icons.search,
            color: Colors.white,
          ),
          SizedBox(
            width: 8,
          ),
        ],
      ),
      height: 30,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12.0),
        color: const Color(0xff82a0c8),
      ),
    );
  }

  Widget _getDropdownWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      height: 30,
      child: DropdownButton(
        underline: Text(""),
        hint: Text(
          'Filter',
          style: TextStyle(
            fontSize: 10,
            color: const Color(0xfff6f6f6),
            fontWeight: FontWeight.w600,
          ),
        ),

        // Not necessary for Option 1
        value: _selectedFiltered,
        style: TextStyle(
          fontSize: 10,
          color: const Color(0xfff6f6f6),
          fontWeight: FontWeight.w600,
        ),
        dropdownColor: Theme.of(context).primaryColor,
        onChanged: changeFilter,
        items: _filters.map((filter) {
          return DropdownMenuItem(
            child: new Text(filter),
            value: filter,
          );
        }).toList(),
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12.0),
        color: const Color(0xff82a0c8),
      ),
    );
  }

  changeFilter(newValue) {
    setState(() {
      _selectedFiltered = newValue;
    });
    // ignore: close_sinks
    final babiesBloc = BlocProvider.of<BabiesBloc>(context);
    if (_selectedFiltered == _filters[0]) {
      babiesBloc.add(GetListOfBabies());
    } else if (_selectedFiltered == _filters[1]) {
      babiesBloc
          .add(GetFilteredListOfBabies(babyState: StateOfBaby.discharged));
    } else if (_selectedFiltered == _filters[2]) {
      babiesBloc.add(GetFilteredListOfBabies(babyState: StateOfBaby.problem));
    } else if (_selectedFiltered == _filters[3]) {
      babiesBloc.add(GetFilteredListOfBabies(babyState: StateOfBaby.normal));
    } else if (_selectedFiltered == _filters[4]) {
      babiesBloc.add(GetFilteredListOfBabies(babyState: StateOfBaby.highRisk));
    }
  }
}

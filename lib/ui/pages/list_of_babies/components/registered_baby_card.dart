import 'package:eceb_prototype/data/models/baby.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class RegisteredBabyCard extends StatelessWidget {
  final String parent;
  final String location;
  final String gender;
  final int hoursFromBirth;
  final StateOfBaby stateOfBaby;

  RegisteredBabyCard(
      {@required this.parent,
      @required this.location,
      @required this.gender,
      @required this.hoursFromBirth,
      @required this.stateOfBaby});

  Color _getContainerColor() {
    if (stateOfBaby == StateOfBaby.normal) {
      return Color(0xffffffff);
    } else if (stateOfBaby == StateOfBaby.problem) {
      return Color(0xffFED604);
    } else if (stateOfBaby == StateOfBaby.highRisk) {
      return Color(0xffEA6165);
    } else if (stateOfBaby == StateOfBaby.discharged) {
      return Color(0xff3D9A4F);
    } else {
      return Color(0xffffffff);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 150,
      color: _getContainerColor(),
      padding: EdgeInsets.symmetric(vertical: 16),
      child: Center(
        child: Container(
          width: MediaQuery.of(context).size.width * .9,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12.0),
            color: const Color(0xffffffff),
            boxShadow: [
              BoxShadow(
                color: const Color(0x29000000),
                offset: Offset(0, 9),
                blurRadius: 28,
              ),
            ],
          ),
          child: Column(
            children: [
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(14.0),
                  color: const Color(0x42e86064),
                ),
                padding: EdgeInsets.all(8),
                child: Text(
                  '$hoursFromBirth Hours from Birth',
                  style: TextStyle(
                    fontSize: 13,
                    color: const Color(0xff000000),
                    fontWeight: FontWeight.w500,
                  ),
                  textAlign: TextAlign.left,
                ),
              ),
              SizedBox(
                height: 0,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600),
                                  text: "Baby ",
                                ),
                                TextSpan(
                                  style: TextStyle(
                                    color: Colors.black87,
                                    fontSize: 11,
                                  ),
                                  text: "of ",
                                ),
                                TextSpan(
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600),
                                  text: "$parent",
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          RichText(
                            text: TextSpan(
                              children: [
                                TextSpan(
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 10,
                                  ),
                                  text: "Location: ",
                                ),
                                TextSpan(
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 10,
                                      fontWeight: FontWeight.w600),
                                  text: "$location",
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        children: [
                          SvgPicture.asset(
                            gender == "Female"
                                ? "assets/images/girl.svg"
                                : "assets/images/boy.svg",
                            height: 30,
                          ),
                          Text(
                            '$gender',
                            style: TextStyle(
                              fontSize: 10,
                              color: const Color(0xff000000),
                              fontWeight: FontWeight.w700,
                            ),
                            textAlign: TextAlign.left,
                          )
                        ],
                      ),
                    ),
                    Icon(Icons.chevron_right),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

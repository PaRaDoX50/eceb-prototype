import 'package:eceb_prototype/ui/pages/home_page/components/on_call_doctor_card.dart';
import 'package:eceb_prototype/ui/pages/home_page/components/summary_card.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Summary of 24 hours',
                style: TextStyle(
                  fontSize: 15,
                  color: const Color(0x75333348),
                  fontWeight: FontWeight.w600,
                ),
                textAlign: TextAlign.left,
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SummaryCard(
                    color: Color(0xff82A0C8),
                    text: "Admitted:",
                    number: 16,
                  ),
                  SummaryCard(
                    color: Color(0xff9F9F9F),
                    text: "Discharged:",
                    number: 22,
                  ),
                  SummaryCard(
                    color: Color(0xffFF6B6B),
                    text: "High Risk:",
                    number: 3,
                  ),
                ],
              ),
            ],
          ),
        ),
        Expanded(
          child: Container(
            color: Color(0xff82A0C8),
            width: MediaQuery.of(context).size.width,

            child: Center(
              child: Container(
                width: MediaQuery.of(context).size.width * .77,
                height: MediaQuery.of(context).size.height*.2,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical:8.0),
                    child: RaisedButton.icon(

                      icon: SvgPicture.asset("assets/images/register.svg"),
                      elevation: 0,
                      label: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: FittedBox(child: Text("To Register a Baby")),
                      ),
                      onPressed: () {},
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          side: BorderSide(color: const Color(0xffececec), width: 1)),
                      color:  Colors.white,
                    ),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25.0),
                    boxShadow: [
                      BoxShadow(
                        color: const Color(0x33000000),
                        offset: Offset(0, 9),
                        blurRadius: 28,
                      ),
                    ],
                  ),
                ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 16),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'On-Call Doctors',
                style: TextStyle(
                  fontSize: 15,
                  color: const Color(0x75333348),
                  fontWeight: FontWeight.w600,
                ),
                textAlign: TextAlign.left,
              ),
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  OnCallDoctorCard(imageName: "doctor_andrea.jpg",name: "Andrea",),
                  OnCallDoctorCard(imageName: "doctor_kim.jpg",name: "Kim",),
                  OnCallDoctorCard(imageName: "doctor_jane.png",name:"Jane"),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}

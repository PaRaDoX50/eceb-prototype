import 'package:flutter/material.dart';

class OnCallDoctorCard extends StatelessWidget {
  final String name;
  final String imageName;

  OnCallDoctorCard({this.imageName, this.name});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width * .23,
          height: MediaQuery.of(context).size.width * .23,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.elliptical(9999.0, 9999.0)),
            image: DecorationImage(
              image: AssetImage('assets/images/$imageName'),
              fit: BoxFit.cover,
            ),
            border: Border.all(width: 3.0, color: const Color(0xff82a0c8)),
          ),
        ),
        SizedBox(
          height: 8,
        ),
        FittedBox(
          child: Text(
            name,
            style: TextStyle(
              color: const Color(0xff333348),
              fontWeight: FontWeight.w600,
            ),
            textAlign: TextAlign.left,
          ),
        ),
        SizedBox(
          height: 4,
        ),
        Text(
          'Online',
          style: TextStyle(
            fontSize: 9,
            color: const Color(0xff064294),
            fontWeight: FontWeight.w300,
          ),
          textAlign: TextAlign.left,
        ),
      ],
    );
  }
}

// Adobe XD layer: 'Registered_nurse' (shape)

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SummaryCard extends StatelessWidget {
  final Color color;
  final String text;
  final int number;

  SummaryCard({this.color, this.text, this.number});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * .25,
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 16),
      child: Column(
        children: [
          SvgPicture.asset(
            "assets/images/baby.svg",
            fit: BoxFit.fill,
            color: color,
          ),
          SizedBox(
            height: 8,
          ),
          FittedBox(
            child: Text(
              text,
              style: TextStyle(
                fontSize: 11,
                color: const Color(0xff333348),
                fontWeight: FontWeight.w600,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            number.toString(),
            style: TextStyle(
              fontSize: 21,
              color: const Color(0xff333348),
              fontWeight: FontWeight.w600,
            ),
            textAlign: TextAlign.left,
          ),
        ],
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10.0),
        color: const Color(0xffffffff),
        boxShadow: [
          BoxShadow(
            color: const Color(0x2e000000),
            offset: Offset(0, 9),
            blurRadius: 28,
          ),
        ],
      ),
    );
  }
}

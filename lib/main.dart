import 'dart:io';

import 'package:device_preview/device_preview.dart';
import 'package:eceb_prototype/business_logic/blocs/babies/babies_bloc.dart';
import 'package:eceb_prototype/data/repositories/babies_repository.dart';
import 'package:eceb_prototype/ui/screens/login_facility/LoginFacility.dart';
import 'package:eceb_prototype/ui/screens/login_individual/LoginIndividual.dart';
import 'package:eceb_prototype/ui/screens/login_type/LoginType.dart';
import 'package:eceb_prototype/ui/screens/main_screen/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() {
  runApp(DevicePreview(
    enabled: false,
    builder: (context) => MyApp(), // Wrap your app
  ),);
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: const Color(0xff82A0C8),
      statusBarIconBrightness: Brightness.light
    ));
    return BlocProvider(
      create: (_)=>BabiesBloc(babiesRepository: BabiesRepository())..add(GetListOfBabies()),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        locale: DevicePreview.locale(context), // Add the locale here
        builder: DevicePreview.appBuilder,
        title: 'Flutter Demo',
        theme: ThemeData(
            primaryColor: const Color(0xff82A0C8),
            visualDensity: VisualDensity.adaptivePlatformDensity,
          fontFamily: 'Poppins'
        ),
        home: LoginType(),
        routes: {
          MainScreen.route:(_)=>MainScreen(),
          LoginIndividual.route:(_)=>LoginIndividual(),
          LoginFacility.route:(_)=>LoginFacility(),
        },
      ),
    );
  }
}

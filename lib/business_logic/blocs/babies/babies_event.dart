part of 'babies_bloc.dart';

@immutable
abstract class BabiesEvent extends Equatable {

}

class GetListOfBabies extends BabiesEvent{
  @override
  // TODO: implement props
  List<Object> get props => [];

}
class GetFilteredListOfBabies extends BabiesEvent{
  final StateOfBaby babyState;
  GetFilteredListOfBabies({@required this.babyState});

  @override
  List<Object> get props => [];

}


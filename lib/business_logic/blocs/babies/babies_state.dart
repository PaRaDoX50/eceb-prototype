part of 'babies_bloc.dart';

@immutable
abstract class BabiesState extends Equatable{
  const BabiesState();

  @override
  List<Object> get props => [];

}
class Uninitialized extends BabiesState{
}
class BabiesListRetrievedState extends BabiesState {

  final List<Baby> babies;
  const BabiesListRetrievedState({this.babies});

  @override
  // TODO: implement props
  List<Object> get props => [];

}
class Loading extends BabiesState{


}


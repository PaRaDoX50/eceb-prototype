import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:eceb_prototype/data/models/baby.dart';
import 'package:eceb_prototype/data/repositories/babies_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'babies_event.dart';

part 'babies_state.dart';

class BabiesBloc extends Bloc<BabiesEvent, BabiesState> {
  final BabiesRepository _babiesRepository;

  BabiesBloc({@required BabiesRepository babiesRepository})
      : assert(babiesRepository != null),
        _babiesRepository = babiesRepository,
        super(Uninitialized());

  @override
  Stream<BabiesState> mapEventToState(BabiesEvent event) async* {
    if (event is GetListOfBabies) {
      yield Loading();
      List<Baby> babies = await _babiesRepository.getListOfBabies();
      if (babies != null) {
        yield BabiesListRetrievedState(babies: babies);
      }
    }
    else if(event is GetFilteredListOfBabies){
      yield Loading();
      List<Baby> babies = await _babiesRepository.getListOfBabies();

      List<Baby> filteredList = babies.where((element) => element.stateOfBaby == event.babyState).toList();
      if (babies != null) {
        yield BabiesListRetrievedState(babies: filteredList);
      }
    }
  }
}
